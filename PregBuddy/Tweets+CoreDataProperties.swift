//
//  Tweets+CoreDataProperties.swift
//  
//
//  Created by Vishnu Khaitan on 06/03/18.
//
//

import Foundation
import CoreData


extension Tweets {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tweets> {
        return NSFetchRequest<Tweets>(entityName: "Tweets")
    }

    @NSManaged public var bookmarked: Bool
    @NSManaged public var favorite_count: Int64
    @NSManaged public var icon: String?
    @NSManaged public var retweet_count: Int64
    @NSManaged public var text: String?
    @NSManaged public var id_str: String?

}
