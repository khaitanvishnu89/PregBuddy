//
//  PBTweetModel.swift
//  PregBuddy
//
//  Created by Vishnu Khaitan on 06/03/18.
//  Copyright © 2018 Vishnu Khaitan. All rights reserved.
//

import UIKit
import ObjectMapper

class PBTweetModel: Mappable {
    
    var text                : String = ""
    var retweet_count       : Int64 = 0
    var favorite_count      : Int64 = 0
    var id_str              : String = ""
  
    
    
    
    init() {}
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    
    func mapping(map: Map) {
        
       
        text                 <- map["text"]
        retweet_count        <- map["retweet_count"]
        favorite_count       <- map["favorite_count"]
        id_str               <- map["id_str"]
      
    }
}




