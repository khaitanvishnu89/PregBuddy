//
//  PBResultModel.swift
//  PregBuddy
//
//  Created by Vishnu Khaitan on 06/03/18.
//  Copyright © 2018 Vishnu Khaitan. All rights reserved.
//

import UIKit
import ObjectMapper

class PBResultModel: Mappable {
    
    var search_metadata      : AnyObject?
    var statuses             : [PBTweetModel]?
  
    init() {}
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    
    func mapping(map: Map) {
    
        search_metadata     <- map["search_metadata"]
        statuses            <- map["statuses"]
      
    }
}





