//
//  CoreDataHelper.swift
//  PregBuddy
//
//  Created by Vishnu Khaitan on 06/03/18.
//  Copyright © 2018 Vishnu Khaitan. All rights reserved.
//

import UIKit
import CoreData

class CoreDataHelper: NSObject {

    static let sharedInstance: CoreDataHelper = {
        let instance = CoreDataHelper()
        // setup code
        return instance
    }()
    
    
    func syncData(tweets : [PBTweetModel]){
        for obj in tweets{
           self.savedata(tweet: obj)
        }
    }

    
    func savedata(tweet : PBTweetModel){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).databaseContext
        let tweetobj : Tweets = NSEntityDescription.insertNewObject(forEntityName: "Tweets", into: context) as! Tweets
        tweetobj.text = tweet.text
        tweetobj.favorite_count  = tweet.favorite_count
        tweetobj.retweet_count = tweet.retweet_count
        tweetobj.id_str = tweet.id_str
        tweetobj.bookmarked = false
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
      }
    
    
    func fetchDataWith(offset : Int)->[Tweets]? {
                print("offset **** ",offset)
                let context = (UIApplication.shared.delegate as! AppDelegate).databaseContext
                let request: NSFetchRequest<Tweets> = Tweets.fetchRequest()
                request.fetchLimit = 20
                request.fetchOffset = offset
        
                do {
        
                   let items = try context.fetch(request)
                   return items
                    // success ...
                } catch let error as NSError {
                    // failure
                   return nil
                }
    }
    
    func isbookMarked(tweet : PBTweetModel)->Bool {
        
                let context = (UIApplication.shared.delegate as! AppDelegate).databaseContext
        
                let request: NSFetchRequest<Tweets> = Tweets.fetchRequest()
                request.predicate = NSPredicate(format: "id_str == %@", tweet.id_str)
        
                do {
                 let items = try context.fetch(request)
                    if(items.count > 0){
                       return true
                    }
                    return false
                } catch let error as NSError {
                    // failure
                    return false
                    print("Fetch failed: \(error.localizedDescription)")
                }
    }
    
    
    func getMostRetweetedTweets()->[Tweets]?{
        
        let sectionSortDescriptor = NSSortDescriptor(key: "retweet_count", ascending: false)
        let sortDescriptors = [sectionSortDescriptor]
        
        
        let context = (UIApplication.shared.delegate as! AppDelegate).databaseContext
        let request: NSFetchRequest<Tweets> = Tweets.fetchRequest()
        request.sortDescriptors = sortDescriptors
        request.fetchLimit = 10
        
        do {
            
            let items = try context.fetch(request)
            return items
            // success ...
        } catch let error as NSError {
            // failure
            return nil
        }
        
    }
    
    func getBookmarkedTweets()->[Tweets]?{
    
        let predicate = NSPredicate(format: "bookmarked == %@", NSNumber(value: true))
        let context = (UIApplication.shared.delegate as! AppDelegate).databaseContext
        let request: NSFetchRequest<Tweets> = Tweets.fetchRequest()
        request.predicate = predicate
        
        do {
            
            let items = try context.fetch(request)
            return items
            // success ...
        } catch let error as NSError {
            // failure
            return nil
        }
        
    }
    
    
    func getMostLikedTweets()->[Tweets]?{
        
        let sectionSortDescriptor = NSSortDescriptor(key: "favorite_count", ascending: false)
        let sortDescriptors = [sectionSortDescriptor]
        
        
        let context = (UIApplication.shared.delegate as! AppDelegate).databaseContext
        let request: NSFetchRequest<Tweets> = Tweets.fetchRequest()
        request.sortDescriptors = sortDescriptors
        request.fetchLimit = 10
        
        do {
            
            let items = try context.fetch(request)
            return items
            // success ...
        } catch let error as NSError {
            // failure
            return nil
        }
        
    }
    
    
    func deleteAllRecords() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Tweets")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    func saveContaxt(){
       (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    


}
