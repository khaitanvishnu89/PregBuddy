//
//  Constant.swift
//  PregBuddy
//
//  Created by Vishnu Khaitan on 06/03/18.
//  Copyright © 2018 Vishnu Khaitan. All rights reserved.
//

import UIKit

#if DEBUG
let defaultURL = ""
#else
let defaultURL = ""
#endif

let TWT_CONSUMER_KEY: String = "mG4KUaS6GkQ5NV98TWdtleM7x"

let TWT_CONSUMER_SECRET: String = "LjbN7K4RPIdXHRkYOUxp4VQysZgTatAMAKPGJErUcNXSE8PlfJ"

let APP_VERSION: String = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate

let documents : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]

let prefrence =  UserDefaults.standard

let DEVICE_BOUNDS : CGRect = UIScreen.main.bounds

let BASE_WIDTH    : CGFloat = 320.00

let ASPECT_RATIO    : CGFloat = DEVICE_BOUNDS.width/BASE_WIDTH

struct PBConfig{
    
    struct ServiceURL{
        
        static let SEARCH_TWEETS : String  =   "https://api.twitter.com/1.1/search/tweets.json"
    }
    
    struct Colors{
        
       
    }
    
    struct Fonts{
        
        
    }
    
}



class Constant: NSObject {
    
    
}

extension UIFont {
    func bold() -> UIFont {
        let descriptor = self.fontDescriptor.withSymbolicTraits(UIFontDescriptorSymbolicTraits.traitBold)
        return UIFont(descriptor: descriptor!, size: 0)
    }
}
