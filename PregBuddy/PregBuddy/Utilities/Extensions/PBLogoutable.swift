//
//  PBLogoutable.swift
//  PregBuddy
//
//  Created by Vishnu Khaitan on 06/03/18.
//  Copyright © 2018 Deeksha Khaitan. All rights reserved.
//

import UIKit


protocol Logoutable {
    func logout()
}

extension Logoutable where Self : PBBaseViewController {
    func logout() {
        //Perform Logout
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionSheetController.addAction(cancelActionButton)
        
        let logout: UIAlertAction = UIAlertAction(title: "LogOut", style: .destructive)
        { action -> Void in
            let store = TWTRTwitter.sharedInstance().sessionStore
            if let userID = store.session()?.userID {
                store.logOutUserID(userID)
                CoreDataHelper.sharedInstance.deleteAllRecords()
                self.navigationController?.popViewController(animated: false)
            }
        }
        actionSheetController.addAction(logout)
        self.present(actionSheetController, animated: true, completion: nil)
        
    }
}
