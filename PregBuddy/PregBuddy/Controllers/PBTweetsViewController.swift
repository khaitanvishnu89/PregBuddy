//
//  PBTweetsViewController.swift
//  PregBuddy
//
//  Created by Vishnu Khaitan on 06/03/18.
//  Copyright © 2018 Vishnu Khaitan. All rights reserved.
//

import UIKit

class PBTweetsViewController: PBBaseViewController,Logoutable {

    var tweets : [Tweets]?
    var page : Int = 0
    var isbookmarkSelected = false
    var isFilterSelected = false
    @IBOutlet weak var bookMarkBtn : UIButton!
    @IBOutlet weak var tweetsTable : UITableView!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tweetsTable.reloadData()
        self.retriveTweets(page: page)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func retriveTweets(page : Int){
        let data : [Tweets]! = CoreDataHelper.sharedInstance.fetchDataWith(offset: page)
        if self.tweets != nil{
            self.tweets!.append(contentsOf: data as [Tweets]!)
        }
        else{
            self.tweets = data as [Tweets]!
        }
        self.tweetsTable.reloadData()
    }
    
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func onBookmarkClick(sender : UIButton){
        if(isbookmarkSelected == false){
            self.bookMarkBtn.setImage(UIImage(named : "bookmark_selected"), for: UIControlState.normal)
            self.tweets = CoreDataHelper.sharedInstance.getBookmarkedTweets()
            self.isFilterSelected = true
            self.isbookmarkSelected = true
            self.tweetsTable.reloadData()
        }
        else{
            self.clearFilter()
            self.resetBookMark()
        }
    }
    
    
    @IBAction func onFilterClick(sender : UIButton){
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: nil, message: "Option to select", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let mostLiked = UIAlertAction(title: "Most Liked ", style: .default)
        { _ in
            print("Most Liked")
            self.sortByMostLiked()
        }
        actionSheetControllerIOS8.addAction(mostLiked)
        
        let mostRetweeted = UIAlertAction(title: "Most Retweeted ", style: .default)
        { _ in
            print("Most Retweeted")
            self.sortByMostTweeted()
        }
        actionSheetControllerIOS8.addAction(mostRetweeted)
        
        let clearFilter = UIAlertAction(title: "Clear Filter ", style: .default)
        { _ in
            
            self.clearFilter()
            print("Clear Filter")
        }
        actionSheetControllerIOS8.addAction(clearFilter)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    
    func sortByMostLiked(){
        self.isFilterSelected = true
        self.tweets = CoreDataHelper.sharedInstance.getMostLikedTweets()
        self.tweetsTable.reloadData()
        self.resetBookMark()
    }
    
    func sortByMostTweeted(){
        self.isFilterSelected = true
        self.tweets = CoreDataHelper.sharedInstance.getMostRetweetedTweets()
        self.tweetsTable.reloadData()
        self.resetBookMark()
    }
    
    func clearFilter(){
        self.isFilterSelected = false
        self.page = 0
        self.tweets?.removeAll()
        self.retriveTweets(page: self.page)
        self.resetBookMark()
    }
    
    
    func resetBookMark(){
        self.bookMarkBtn.setImage(UIImage(named : "bookmark"), for: UIControlState.normal)
        self.isbookmarkSelected = false
    }
    
    
    @IBAction func onClickLogout(sender : UIButton){
     self.logout()
    }
    

}


extension PBTweetsViewController : UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - TableView DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let tweets = self.tweets{
            if(self.isFilterSelected == false){
           return tweets.count + 1
            }
            else{
            return tweets.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == self.tweets?.count){
            let cell = tableView.dequeueReusableCell(withIdentifier: "PBLoadMoreCell", for: indexPath) as! PBLoadMoreCell
            return cell
        }
        else{
        let tweet = self.tweets![indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "PBTweetCell", for: indexPath) as! PBTweetCell
        cell.tweet = tweet
        cell.delegate = self
        cell.setupCell()
        return cell
        }
    }
    
    // MARK: - TableView Delegate Methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == self.tweets?.count){
           self.page =  self.page + 1
           self.retriveTweets(page: self.page)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
            return UITableViewAutomaticDimension
       
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
       
         return 40
        
    }
    
   
    
    
}

extension PBTweetsViewController : PBTweetCellDelegate {

    func didselectBookmark(cell : PBTweetCell){
      
        self.tweetsTable.reloadData()
        
    }
}

