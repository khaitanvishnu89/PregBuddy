//
//  ViewController.swift
//  PregBuddy
//
//  Created by Vishnu Khaitan on 06/03/18.
//  Copyright © 2018 Vishnu Khaitan. All rights reserved.
//

import UIKit
import ObjectMapper
import CoreData


class ViewController: UIViewController {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        let store = TWTRTwitter.sharedInstance().sessionStore
        let lastSession = store.session()
        
        if(lastSession != nil){
            self.retrivedata()
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func onLoginButtonClick(sender : UIButton){
        self.requestForLogin()
    }
    
    
    func requestForLogin(){
        
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                print("logged in user with id \(session!.userID),\(session!.userName),\(session!.authToken)")
                self.retrivedata()
            } else {
                print("error: \(error!.localizedDescription)");
            }
        })
    }
    
    
    func retrivedata() {
        // Swift
        let client = TWTRAPIClient()
        let statusesShowEndpoint = PBConfig.ServiceURL.SEARCH_TWEETS
        let params = ["q" :"pregnancy","count" : "100"]
        var clientError : NSError?
        
        let request = client.urlRequest(withMethod: "GET", urlString: statusesShowEndpoint, parameters: params, error: &clientError)
        
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError != nil {
                print("Error: \(String(describing: connectionError))")
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: [])
                let mapperObj = Mapper<PBResultModel>()
                let parsedObject = mapperObj.map(JSONObject: json)!
                CoreDataHelper.sharedInstance.syncData(tweets: parsedObject.statuses!)
                self.pushToTweetsView(result: parsedObject)
            } catch let jsonError as NSError {
                //print("json error: \(jsonError.localizedDescription)")
            }
        }
    }
    
    
    func pushToTweetsView(result : PBResultModel?){
        
        let tweetsView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PBTweetsViewController") as! PBTweetsViewController
        self.navigationController?.pushViewController(tweetsView, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

