//
//  PBTweetCell.swift
//  PregBuddy
//
//  Created by Vishnu Khaitan on 06/03/18.
//  Copyright © 2018 Vishnu Khaitan. All rights reserved.
//

import UIKit

protocol PBTweetCellDelegate {
    func didselectBookmark(cell : PBTweetCell)
}

class PBTweetCell: UITableViewCell {
    
    @IBOutlet weak var tweetText : UITextView!
    @IBOutlet weak var favouriteLbl : UILabel!
    @IBOutlet weak var retweetLbl   : UILabel!
    @IBOutlet weak var bookmarkBtn  : UIButton!
    
    var delegate : PBTweetCellDelegate?
    
    
    var tweet : Tweets?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(){
        if let tweetObj = tweet{
            self.tweetText.text = tweetObj.text
            self.favouriteLbl.text = String(tweetObj.favorite_count)
            self.retweetLbl.text = String(tweetObj.retweet_count)
            
            if tweetObj.bookmarked{
                self.bookmarkBtn.setImage(UIImage(named : "bookmark_selected"), for: UIControlState.normal)
            }
            else{
                self.bookmarkBtn.setImage(UIImage(named : "bookmark"), for: UIControlState.normal)
            }
        }
        
        
    }
    
    @IBAction func onClickBookmark(sender : UIButton){
       if let tweetObj = tweet{
          if tweetObj.bookmarked{
            tweetObj.bookmarked = false
          }
          else{
            tweetObj.bookmarked = true
          }
         CoreDataHelper.sharedInstance.saveContaxt()
         self.delegate?.didselectBookmark(cell: self)
    }
    }

}
